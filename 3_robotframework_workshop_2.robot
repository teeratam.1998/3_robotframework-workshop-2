*Settings*
Library  BuiltIn
Library  SeleniumLibrary
Library  OperatingSystem

*Variables*

${url}=  http://ptt-devpool-robotframework.herokuapp.com/
${input}=  RobotFramework
${id}=  admin
${pass}=  1234
&{0}=  firstname=David  lastname=Beckham  email=David.Beckham@gmail.com  1=1  2=2  3=3
&{1}=  firstname=Johnny  lastname=Deep  email=Johnny.Deep@gmail.com
&{2}=  firstname=Robert  lastname=Downey  email=Robert.Downey@gmail.com
&{3}=  firstname=Tom  lastname=Hank  email=Tom.Hank@gmail.com
${item}=  0
*Test Cases*

1.Open Broser
   
    Create Webdriver  Chrome  executable_path=C:/Program Files (x86)/Google/Chrome/Application/chromedriver.exe
    Open Browser  about:blank  browser=Chrome
    Maximiza Browser Window
    Set Selenium Speed  0.5
2.go to google
    Go To  ${url}
3.insert Robot in search bar
    Input Text  id=txt_username  ${id}
    Input Text  id=txt_password  ${pass}

4.click submit
    Click Button  id=btn_login

# 5. click add
#     Click Button  id=btn_new
5. insert data
     FOR  ${i}  IN RANGE  0  4
    Click Button  id=btn_new
    Input Text  id=txt_new_firstname  ${${i}}[firstname]
    Input Text  id=txt_new_lastname  ${${i}}[lastname]
    Input Text  id=txt_new_email  ${${i}}[email]
    Click Button  id=btn_new_save
    END
# 7. submit data
#     Click Button  id=btn_new_save
6. Screenshot
    Capture Page Screenshot  filename=${CURDIR}/screens.PNG
# 7. Close Browser
#     Close All Browsers